package scala

import scalafx.application.JFXApp3
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.layout.HBox
import javafx.scene.paint.Color._
import scalafx.scene.paint._
import scalafx.scene.text.Text
import scalafx.animation.AnimationTimer
import scala.collection.parallel.CollectionConverters.*
import scalafx.Includes._

import scalafx.scene.Node
import scalafx.scene.shape.Rectangle
import scalafx.collections.ObservableBuffer
import scalafx.beans.binding.SetBinding
import scalafx.collections.ObservableBuffer.Change
import scalafx.collections.ObservableBuffer.Add
import scalafx.collections.ObservableBuffer.Remove
import scalafx.collections.ObservableBuffer.Reorder
import scalafx.collections.ObservableBuffer.Update
import scala.collection.mutable.Buffer
import scalafx.beans.property.ObjectProperty
import scala.util.Random
import blocks.*
import scala.Physics.*
import scalafx.beans.value.ObservableValue
import blocks.Block.allBlocks
import scala.Toolbar.*
import scalafx.scene.layout.BorderPane
import scalafx.scene.layout.Border
import scala.Settings
import scala.Settings.given
import scalafx.scene.input.KeyEvent
import scalafx.scene.input.MouseEvent
import scalafx.scene.input.ScrollEvent
import blocks.Block.NUMBER_BLOCKS
import scalafx.scene.input.KeyCode
import scalafx.scene.layout.StackPane
import javax.tools.Tool
import scalafx.scene.layout.Pane

object Main extends JFXApp3 {

  type World = Vector[ObjectProperty[Block]] // only elements that change will be notified

  def keyTypedListener(env: World): KeyEvent => Unit = e =>
    if e.getCharacter() == " " then env.foreach(_.set(allBlocks(selectedBlockProperty.get()))) else
      e.getCharacter().toIntOption match
      case None    => ()
      case Some(value) => if value < Block.NUMBER_BLOCKS then
          Toolbar.selectedBlockProperty.set(value)
    
  def keyPressedListener: KeyEvent => Unit = e =>    

    if e.code.equals(KeyCode.LEFT) then
      selectedBlockProperty.set((selectedBlockProperty.get() + NUMBER_BLOCKS - 1) % NUMBER_BLOCKS)
    else if e.code.equals(KeyCode.RIGHT) then
      selectedBlockProperty.set((selectedBlockProperty.get() + 1) % NUMBER_BLOCKS)

  def scrollListener: ScrollEvent => Unit =
    e => selectedBlockProperty.set((selectedBlockProperty.get() + NUMBER_BLOCKS + 1 * e.getDeltaY().sign.toInt) % NUMBER_BLOCKS)
    
  def buildStage(env: World)(using s: Settings): PrimaryStage =
    val sandboxWithToolTip = new StackPane:
      children.add(SandBox.pane(env))
      children.add(Toolbar.currentlySelectedTooltip)
      


    val mainScene = new Scene { 
      onKeyTyped = keyTypedListener(env)
      onScroll = scrollListener
      onKeyPressed = keyPressedListener
      
      content = 
        new BorderPane{
          center = sandboxWithToolTip
          bottom = Toolbar.pane
          border = Border.stroke(Color.Black)
        }
    }

    new PrimaryStage {
      title = "sandfall"
      width = s.windowWidth
      height = s.windowHeight + Toolbar.HEIGHT
      scene = mainScene
    }


  override def start(): Unit =

    val s = summon[Settings]

    val world: World = Vector.from(
      Array.tabulate(s.height * s.width)(_ => 
        val air: ObjectProperty[Block] = ObjectProperty(Air())
        air
      ))

    buildStage(world)
 

    var lastFrame: Long = 0
    AnimationTimer: time =>
      if time - lastFrame > 1_000_000_000 / 60 then
        lastFrame = time
        step(world)
    .start()

}
