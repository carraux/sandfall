package scala

import scalafx.Includes.*
import scalafx.beans.property.IntegerProperty
import scalafx.scene.layout.Pane
import scalafx.scene.paint.Color
import scalafx.scene.layout.Background
import scalafx.scene.input.KeyEvent
import blocks.Block
import blocks.Block.allBlocks
import scala.Main.World
import scalafx.scene.shape.Rectangle
import scalafx.scene.layout.HBox
import scalafx.scene.Node
import scalafx.geometry.Pos
import scalafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import scalafx.scene.layout.TilePane
import scalafx.geometry.Pos.Center
import scalafx.geometry.Insets
import scalafx.scene.layout.FlowPane
import javafx.scene.layout.Border
import scalafx.scene.layout.BorderStroke
import scalafx.scene.paint.Color.*
import scalafx.beans.property.StringProperty
import scalafx.beans.binding.Bindings
import scalafx.scene.text.Text
import scalafx.geometry.Pos.BottomCenter

/**
  * Over Screen Display:
    gui tool: selected block, selectable blocks, brush size, etc
  */
object Toolbar {
  val selectedBlockProperty = IntegerProperty(0)

  final val HEIGHT = 100//OSD height
  final val PREFLEN = HEIGHT - 20
  final val BLOCK_LEN = 40

  def currentlySelectedTooltip: Node = 
    val str = selectedBlockProperty.map(idx => s"selected: ${allBlocks(idx.intValue()).getClass().getSimpleName()}")

    val tooltip = new Text
    tooltip.textProperty().bind(str)
    tooltip.alignmentInParent = BottomCenter
    tooltip




  def pane: Pane = 
    val p = new TilePane{
      prefHeight = HEIGHT
      background = Background.fill(Color.LightGray)
      alignment = Center
      hgap = 5
    }

    val items = allBlocks.zipWithIndex.map((block, idx) =>
      new FlowPane:
        prefWidth = PREFLEN
        prefHeight = PREFLEN
        alignment = Center
        border.bind(when(selectedBlockProperty.isEqualTo(idx)).choose(Border.stroke(Black)).otherwise(Border.stroke(White)))
        children = Rectangle(BLOCK_LEN, BLOCK_LEN, block.colorProperty.get())
        onMousePressed = _ => selectedBlockProperty.set(idx)
    )

    

    p.children = items

    p
}
