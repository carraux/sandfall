package scala

case class Settings(windowWidth: Int, windowHeight: Int, res: Int):
    val width = windowWidth / res
    val height = windowHeight / res

given Settings = Settings(1000, 800, 10)