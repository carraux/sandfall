package blocks

import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scala.Main.World
import scala.Physics.*

import Direction.*
import scalafx.beans.property.ObjectProperty

import scalafx.beans.value.ObservableValue
import javafx.scene

    


final class Sand extends Block:
    override val colorProperty: ObservableValue[scene.paint.Color, scene.paint.Color] = ObjectProperty(YELLOW)

    val s = summon[Settings]

    override def update(idx: Int, env: World): Unit = 
        if env.definedAt(idx, S) && env.traversableAt(idx + s.width) then //swap both
            env.swap(idx, idx + s.width) else 
        if env.definedAt(idx, SE) && env.traversableAt(idx + s.width + 1) && env.traversableAt(idx + 1) then
            env.swap(idx, idx + s.width + 1) else 
        if env.definedAt(idx, SW) && env.traversableAt(idx + s.width - 1) && env.traversableAt(idx - 1) then
            env.swap(idx, idx + s.width - 1)
        

