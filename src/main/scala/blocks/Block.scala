package blocks


import scalafx.Includes.*

import Main.World
import scala.Physics.*
// import scalafx.beans.property.ObjectProperty

import scalafx.beans.property.IntegerProperty
// import scalafx.beans.binding.ObjectBinding
// import scalafx.beans.binding.Bindings
// import scalafx.beans.value.ObservableValue
import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scalafx.beans.value.ObservableValue
import scalafx.beans.binding.Bindings
import scala.util.Random

object Block:
  def allBlocks = List(Air(), Sand(), Water(), Stone(), Wood(), Leaf(), Lava(), Paper(), GunPowder())
  val NUMBER_BLOCKS = allBlocks.length



trait Block:
  def update(idx: Int, env: World): Unit = ()

  val colorProperty: ObservableValue[Color, Color]

  //final def color: Color = colorProperty.get()


trait Traversable extends Block:
 /**
   * volumic mass. you can only traverse another traversable if your volumic mass is stronger
   *
   * @return the volumic mass
   */
  def rho: Int

  protected def canTraverse(b: Block): Boolean = b match
    case t: Traversable => t.rho < rho
    case _ => false

    

trait Ignitable(startHP: Int, necessaryHeat: Int) extends Block:
  private var hp = startHP
  private val heatProperty: IntegerProperty = IntegerProperty(0)

  val ignited = heatProperty >= necessaryHeat
  def life: Int = hp

  def warm(h: Int): Unit = 
    heatProperty.set(heatProperty.get() + h)
    if h < 0 && heatProperty.get() < -50 then heatProperty.set(-50)

  private val WARM_OTHER_SPEED = 2
  private val BURN_SPEED = 5

  protected def defaultColor: Color

  private def burningColor: Color = List(ORANGE,ORANGE,ORANGE,ORANGE,RED,ORANGERED,ORANGERED,ORANGERED,ORANGERED, YELLOW, LIGHTGRAY)(Random.nextInt(11))


  final override val colorProperty = Bindings.when(ignited).choose(burningColor).otherwise(defaultColor)
  
  //colorProperty.bind(Bindings.)

  override def update(idx: Int, env: World): Unit = 
    if hp <= 0 then env(idx).set(Air())//should be garbage collected

    //colorProperty.set(BlanchedAlmond)

    if ignited.get() then 
      burn
      //warm others
      Direction.values.
      filter(env.definedAt(idx, _)).
      foreach(dir =>
        env(idx + dir.relIdx).get() match
            case i: Ignitable => i.warm(WARM_OTHER_SPEED)
            case _ => ()
      )

    
    


  def burn: Unit = hp -= BURN_SPEED


// extension (env: World)
//   def swap(a: Int, b: Int): Unit =
//     val oldA = env(a).get()
//     val oldB = env(b).get()
//     env(a).set(oldB)
//     env(b).set(oldA)
  
//   def traversableAt(idx: Int): Boolean =
//     env(idx).get().traversable

// def fall(using s: Settings): (Int, World) => Unit = 
//   (idx, env) => if env.isDefinedAt(idx + s.width) && env.traversableAt(idx + s.width) then //swap both
//     env.swap(idx, idx + s.width) else 
//   if env.isDefinedAt(idx + s.width + 1) && env.traversableAt(idx + s.width + 1) && env.traversableAt(idx + 1) then
//     env.swap(idx, idx + s.width + 1) else 
//   if env.isDefinedAt(idx + s.width - 1) && env.traversableAt(idx + s.width - 1) && env.traversableAt(idx - 1) then
//     env.swap(idx, idx + s.width - 1)

// def waterMove(using s: Settings): (Int, World) => Unit =
//   (idx, env) => if env.isDefinedAt(idx) then () else ()



// enum Block(col: Color, trav: Boolean = false, fireDamage: Int = 0, update: (Int, World) => Unit = (_,_) => ()):
//     case Air extends Block(White, trav = true)
//     case Sand extends Block(Yellow, update = fall)
//     case Rock extends Block(DarkGray)
//     case Wood(var life: Int = 1000) extends Block(Maroon, fireDamage = 25)
//     case Leaf(var life: Int = 1000) extends Block(Green, fireDamage = 50)
//     case Water extends Block(Blue, trav = true, update = fall)
    

//     def color = col
//     def traversable = trav

//     def step(idx: Int, env: World) = update(idx, env)



//for the list to be covariant, I need it to be immutable. update the whole list, but as a vector

