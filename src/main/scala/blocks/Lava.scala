package blocks

import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scala.Main.World
import Physics.*
import Physics.Direction.*
import scala.util.Random
import scalafx.beans.property.ObjectProperty
import scalafx.scene.paint

class Lava extends Traversable{

  override val colorProperty = ObjectProperty(RED)

  override def rho: Int = 20

  

  override def update(idx: Int, env: World): Unit =   

    def react(dir: Direction): Boolean =
        env.definedAt(idx, dir) &&
        {
            env(idx + dir.relIdx).get() match
            case _: Water => 
                env(idx).set(Air())
                env(idx + dir.relIdx).set(Stone())
                true
            case t: Traversable => rho > t.rho && {
                env.swap(idx, idx + dir.relIdx)
                true
            }
            case _ => false
        }
        

    //move + possible reaction with water
    react(S) || react(SE) || react(SW) || Random.shuffle(List(E,W)).exists(react)

    //set fire if direct (or edge) contact with ignitable matter
    Direction.values.
    filter(env.definedAt(idx, _)).
    foreach(dir =>
        env(idx + dir.relIdx).get() match
            case i: Ignitable => i.warm(10)
            case _ => ()
    )

            
    

    
}
