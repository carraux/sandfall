package blocks

import scala.Physics.randomLife
import scala.Physics.randomHeat
import javafx.scene.paint.Color.*
import javafx.scene.paint.Color
import scalafx.beans.property.ObjectProperty
import scalafx.scene.paint

class Paper extends Ignitable(randomLife(500), randomHeat(50)){
    override protected def defaultColor = WHITESMOKE

}
