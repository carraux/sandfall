package blocks

import scala.Main.World
import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import Physics.* 
import Physics.Direction.*
import scala.util.Random
import scalafx.beans.property.ObjectProperty
import scalafx.scene.paint

class Water extends Traversable {

  override val colorProperty = ObjectProperty(BLUE)

  override def rho: Int = 10

  private val MAX_WATER_DISTANCE = 3

  override def update(idx: Int, env: World): Unit =

    // def canGo(rel: Int)(using s: Settings): Boolean = 
    //   env(rel).get() match
    //       case _: Lava => true
    //       case t: Traversable => rho > t.rho
    //       case _ => false

    def react(dir: Direction)(using s: Settings): Boolean = 
        env.definedAt(idx, dir) && {env(idx + dir.relIdx).get() match
          case _: Lava => 
            env(idx).set(Air())
            env(idx + dir.relIdx).set(Obsidian())
            true
          case t: Traversable => rho > t.rho && {
            env.swap(idx, idx + dir.relIdx)
            true
          }
          case _ => false
}

    def superReact(dir: SuperDirection)(using s: Settings): Boolean = 
       env.definedAt(idx, dir) && {env(idx + dir.relIdx).get() match
          case _: Lava => 
            env(idx).set(Air())
            env(idx + dir.relIdx).set(Obsidian())
            true
          case t: Traversable => rho > t.rho && {
            env.swap(idx, idx + dir.relIdx)
            true
          }
          case _ => false
}
    //val s = summon[Settings]
  
    // def searchHole(dist: Int, dir: Direction): Int = 
    //  if env.definedAt(dist, dir * dist) 
    //   then if canGo(dir.horizontalMovement * dist + s.width) 
    //     then dist//hole exist
    //     else if canGo(dir.horizontalMovement*dist) && dist + 1 <= MAX_WATER_DISTANCE
    //       then searchHole(dist + 1, dir)//no hole, try to go further
    //       else dist - 1//no hole, blocked
    //   else dist - 1
    //returns the distance to the nearest accessible hole, with cut through corner
    //if no hole in given range, return the furthest distance it can go

    //

      
      

     

    // //1. check if can go down
    // (env.definedAt(idx,S) && react(idx + s.width)) ||
    // //2. check holes (also checks horizontal moves, will automatically go to furthest possible place if no hole)
    // Random.shuffle(List(E,W)).map(dir => (dir, searchHole(1, dir))).maxByOption(_._2).forall((dir, len) => react(idx + dir.horizontalMovement * len))//should work ?
    //       //very bad, maybe because it doesn't fall


            //from dist 1 to MAX, check if there is a hole (LR), priority to nearest hole

            //from dist 1 to MAX, check if reachable (n-1 must be reachable for n to be reachable), random dist
    

    react(S) || 
    react(SE) || 
    react(SW) || {
      val superDir = (1 to MAX_WATER_DISTANCE).toList.//this value should be variable // starts to reach down further and further, prioriziting near
      map(len => List(E,W).map(dir => SuperDirection(dir, len))).
      flatMap(Random.shuffle)//shuffle by direction, but not by length
      superDir.exists(superReact)
    }


    //make ignitable a bit colder 
    Direction.values.
    filter(env.definedAt(idx, _)).
    foreach(dir =>
        env(idx + dir.relIdx).get() match
            case i: Ignitable => i.warm(-1)
            case _ => ()
    )

    // else if env.definedAt(idx, SE) && canTraverse(
    //     env(idx + s.width + 1).get()
    //   ) && canTraverse(env(idx + 1).get())
    // then env.swap(idx, idx + s.width + 1)

    // else if env.definedAt(idx, SW) && canTraverse(
    //     env(idx + s.width - 1).get()
    //   ) && canTraverse(env(idx - 1).get())
    // then env.swap(idx, idx + s.width - 1)

    // //lateral movement -> random left or right

    // else 
    //   val choice = List(E,W).filter(dir =>
    //       env.definedAt(idx, dir) &&
    //       canTraverse(env(idx + dir.horizontalMovement).get())
    //   )
    //   if choice.nonEmpty then
    //     choice.lift(Random.nextInt(choice.length)).foreach(dir => env.swap(idx, idx + dir.horizontalMovement))

    //     if env.definedAt(idx, W) && canTraverse(env(idx - 1).get()) then
    //       env.swap(idx, idx - 1)
    //     else if env.definedAt(idx, E) && canTraverse(env(idx + 1).get()) then
    //       env.swap(idx, idx + 1)
    

}


/* 

!!!!!!!!!!!
IMPLEMENT THE UPDATE BUFFER
!!!!!!!!!!!

WRITE EVERYTHING ON ANOTHER VECTOR, THEN COPY IT


OR 

STORE ALL CHANGES ON A TAPE (LIST OF CHANGE), THEN APPLY THEM


 */ 





