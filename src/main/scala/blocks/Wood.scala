package blocks

import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scala.Main.World
import scala.Physics.randomLife
import scala.Physics.randomHeat
import scalafx.scene.paint

class Wood extends Ignitable(randomLife(2500), randomHeat(600)){
  override protected def defaultColor = MAROON

}
