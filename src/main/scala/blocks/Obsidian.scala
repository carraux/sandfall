package blocks

import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scalafx.beans.property.ObjectProperty
import scalafx.scene.paint

class Obsidian extends Block{
    override val colorProperty = ObjectProperty(PURPLE)
  
}
