package blocks

import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scalafx.beans.property.ObjectProperty
import scalafx.scene.paint


class Stone extends Block{
    override val colorProperty = ObjectProperty(DARKGRAY)
}
