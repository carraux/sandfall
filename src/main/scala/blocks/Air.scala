package blocks

import javafx.scene.paint.Color.*
import javafx.scene.paint.Color
import scalafx.beans.property.ObjectProperty


final class Air extends Traversable:
    override val colorProperty = ObjectProperty(WHITE)

    override def rho: Int = 0
    
