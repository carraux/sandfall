package blocks

import scala.Physics.randomLife
import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import scala.Physics.randomHeat
import scalafx.beans.property.ObjectProperty
import scalafx.scene.paint

class Leaf extends Ignitable(randomLife(1000), randomHeat(100)){
  override protected def defaultColor = GREEN
}
