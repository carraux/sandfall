package scala

import scalafx.scene.layout.Pane
import scalafx.scene.Scene
import scala.Main.World
import scalafx.scene.shape.Rectangle
import blocks.Block.allBlocks
import scala.Toolbar.selectedBlockProperty
import blocks.Block
import scalafx.scene.layout.Background
import scalafx.scene.paint.Color
import scala.Settings.given
import scala.Physics.Position



object SandBox:

  def pane(env: World)(using s: Settings): Pane = new Pane{//might want to change to TilePane


    prefWidth = s.windowWidth
    prefHeight = s.windowHeight

    env.zipWithIndex.foreach((prop, idx) =>
      val pos = Position.byIdx(idx)
      
      children.addOne(new Rectangle:
          x = pos.x * s.res
          y = pos.y * s.res
          width = s.res
          height = s.res

          fill.bind(prop.flatMap(_.colorProperty))

          onMouseMoved = {e =>
          if e.isControlDown() then
              for
                  x <- pos.x - 2 to pos.x + 2
                  y <- pos.y - 2 to pos.y + 2
                  if env.isDefinedAt(y * s.width + x)
              do env(y * s.width + x).set(allBlocks(selectedBlockProperty.get()))
          }
      )
    )


      
}

