package scala

import blocks.*
import scala.Main.World
import scala.Settings.given
import scala.util.Random



object Physics:

  case class Position(x: Int, y: Int):
    override def toString = s"($x,$y)"
  
  extension(idx: Int)
    def +(dir: Direction): Int =
      idx + dir.relIdx

  object Position:
    def byIdx(idx: Int)(using s: Settings): Position =
      Position(idx % s.width, idx / s.width)

  def step(w: World)(using s: Settings): Unit =
    Random.shuffle(w.zipWithIndex).
    foreach((prop, idx) => prop.get().update(idx, w)) 


  enum Direction:
    case N, NW, W, SW, S, SE, E, NE


    def relIdx(using s: Settings) = this match
      case N  => -s.width
      case NW => -s.width - 1
      case W  => -1
      case SW => +s.width - 1
      case S  => +s.width
      case SE => +s.width + 1
      case E  => +1
      case NE => -s.width + 1

    def horizontalMovement: Int = this match
        case N => 0
        case NW => -1
        case W => -1
        case SW => -1
        case S => 0
        case SE => 1
        case E => 1
        case NE => 1
    def *(len: Int): SuperDirection = SuperDirection(this, len)

  case class SuperDirection(dir: Direction, len: Int):
    def relIdx = dir.relIdx*len
    def horizontalMovement: Int = len * dir.horizontalMovement
  
    
    
  def randomLife(mean: Int): Int =
    Random.between(mean / 2, mean * 2)

  def randomHeat(mean: Int): Int =
    Random.between(mean - mean/10, mean + mean/10 + 1)


  extension (env: World)
    def swap(a: Int, b: Int): Unit =
      val oldA = env(a).get()
      val oldB = env(b).get()
      env(a).set(oldB)
      env(b).set(oldA)
      //(b, oldA) :: (a, oldB) :: Nil

    // def applyChange(c: WorldChange): Unit =
    //   env(c._1).set(c._2)
      

    def traversableAt(idx: Int): Boolean =
      env(idx).get() match
        case _: Traversable => true
        case _              => false

    def definedAt(
        idx: Int,
        dir: SuperDirection
    )(using s: Settings): Boolean = 
      env.isDefinedAt(idx + dir.relIdx)//checks that I'm still in the window
      && {
        val hor = dir.horizontalMovement
        val x = idx % s.width + hor
        hor == 0 || //only up or down, was checked before
        0 <= x && x < s.width //left or right, check that doesnt go through a window
      }

    def definedAt(
        idx: Int,
        dir: Direction
    )(using s: Settings): Boolean = 
      env.isDefinedAt(idx + dir.relIdx)//checks that I'm still in the window
      && {
        val hor = dir.horizontalMovement
        val x = idx % s.width + hor
        hor == 0 || //only up or down, was checked before
        0 <= x && x < s.width //left or right, check that doesnt go through a window
      }

          
        

